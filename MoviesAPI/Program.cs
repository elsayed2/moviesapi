using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Text;

var builder = WebApplication.CreateBuilder(args);
var ApplicationConnection = builder.Configuration.GetConnectionString("MoviesApiConnection");
var IdentityConnection = builder.Configuration.GetConnectionString("IdentityConnection");

// Add services to the container.
builder.Services.AddControllers();
builder.Services.AddCors();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddAutoMapper(typeof(Program));

// Registering the GenresService implementation
builder.Services.AddTransient<IGenresService, GenresService>();

// Registering the MoviesService implementation
builder.Services.AddTransient<IMoviesService, MoviesService>();

// Registering the AuthService implementation
builder.Services.AddScoped<IAuthService, AuthService>();

// Add the application's DbContext to the services container
builder.Services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(ApplicationConnection));

// Add the Identity's DbContext to the services container
builder.Services.AddDbContext<IdentityContext>(options => options.UseSqlServer(IdentityConnection));

// Configure JWT settings using the values from the appsettings.json file
builder.Services.Configure<JWT>(builder.Configuration.GetSection("JWT"));

//  Configure Identity services with ApplicationUser and IdentityRole using Entity Framework for data storage in IdentityContext
builder.Services.AddIdentity<ApplicationUser,IdentityRole>().AddEntityFrameworkStores<IdentityContext>();



// Configure Swagger generation for API documentation
builder.Services.AddSwaggerGen(options =>
{
    // Configure the Swagger document for the Movies API
    options.SwaggerDoc("MoviesAPI", new OpenApiInfo
    {
       Version = "v1",
       Title="Movies API",
       Description= "It provides endpoints to manage movies.",
       Contact=new OpenApiContact
       {
           Name="El-Sayed",
           Email="elsayed2440@gmail.com"
       }
    });

    // Configure the Bearer authentication security definition for JWT tokens
    options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
       Name = "Auth",
       Description="Enter your JWT token",
       Type=SecuritySchemeType.ApiKey,
       Scheme="Bearer",
       BearerFormat="JWT",
       In=ParameterLocation.Header
    });
});

// Configure JWT authentication
builder.Services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(o =>
{
    o.RequireHttpsMetadata = false;
    o.SaveToken = false;
    o.TokenValidationParameters = new TokenValidationParameters
    {
        ClockSkew=TimeSpan.Zero,
        ValidateIssuerSigningKey = true,
        ValidateIssuer = true,
        ValidateAudience = true,
        ValidateLifetime = true,
        ValidAudience = builder.Configuration["JWT:Audience"],
        ValidIssuer = builder.Configuration["JWT:Issuer"],
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["JWT:Key"]))
    };
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(options =>
    {
        options.SwaggerEndpoint("/swagger/MoviesAPI/swagger.json", "Movies API V1");
    });
}

app.UseHttpsRedirection();
 
// Enable CORS to allow requests from any origin with any header and method
app.UseCors(c => c.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());


app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
