﻿namespace MoviesAPI.DTOs
{
    public class MovieCreateDTO : MovieBaseDTO
    {

        public IFormFile Poster { get; set; }

    }
}
