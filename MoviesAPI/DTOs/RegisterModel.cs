﻿namespace MoviesAPI.DTOs
{
    public class RegisterModel
    {
        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(25)]
        public string Username { get; set; }
        
        [StringLength(100)]
        public string Email { get; set; }

        [StringLength(75)]
        public string Password { get; set; }

    }
}
