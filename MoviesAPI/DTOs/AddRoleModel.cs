﻿namespace MoviesAPI.DTOs
{
    public class AddRoleModel
    {
        public string UserId { get; set; }
        public string Role { get; set; }
    }
}
