﻿using System.Text.Json.Serialization;

namespace MoviesAPI.DTOs
{
    public class AuthModel
    {
        public string? Username { get; set; }
        public string? Email { get; set; }
        public string? Token { get; set; }
        public List<string>? Roles { get; set; }
        public string Message { get; set; }
        public DateTime? ExpiresOn { get; set; }
        public bool IsAuthenticated { get; set; }

        [JsonIgnore]
        public string? RefreshToken { get; set; }
        public DateTime RefreshTokenExpiration { get; set; }
    }
}
