﻿namespace MoviesAPI.DTOs
{
    public class MovieUpdateDTO:MovieBaseDTO
    {
        public IFormFile? Poster { get; set; }
    }
}
