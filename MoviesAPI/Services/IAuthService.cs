﻿using System.IdentityModel.Tokens.Jwt;

namespace MoviesAPI.Services
{
    public interface IAuthService
    {
        Task<JwtSecurityToken> GenerateTokenAsync(ApplicationUser user);
        Task<AuthModel> RegisterAsync(RegisterModel model);
        Task<AuthModel> GetTokenAsync(TokenRequestModel model);
        Task<string> AddRoleAsync(AddRoleModel model);
        Task<AuthModel> RefreshTokenAsync(string refreshToken);
        Task<bool> RevokeTokenAsync(string token);
    }
}
