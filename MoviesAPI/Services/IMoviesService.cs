﻿namespace MoviesAPI.Services
{
    public interface IMoviesService
    {
        Task<IEnumerable<MovieDetailsDTO>> GetAsync(byte genreId=0);
        Task<MovieDetailsDTO> AddAsync(Movie movie);
        Task<Movie> GetByIdAsync(int id);
        Task<Movie> RemoveAsync(Movie movie);
        Task<Movie> UpdateAsync(Movie movie, MovieUpdateDTO dto);
        Task<MovieDetailsDTO> MovieDetailsAsync(Movie movie);
    }
}
