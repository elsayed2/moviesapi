﻿namespace MoviesAPI.Services
{
    public interface IGenresService
    {
        Task<IEnumerable<Genre>> GetAllAsync();
        Task<Genre> AddAsync(Genre genre);
        Task<Genre> GetByIdAsync(byte id);
        Task<bool> AnyAsync(byte id);
        Genre Update(Genre genre);
        Genre Remove(Genre genre);
    }
}
