﻿namespace MoviesAPI.Services
{
    public class GenresService : IGenresService
    {
        private readonly ApplicationDbContext _context;
        public GenresService(ApplicationDbContext context)
        {
            _context = context;
        }

        // Add a new genre to the database and return the added genre.
        public async Task<Genre> AddAsync(Genre genre)
        {
            await _context.Genres.AddAsync(genre);
            _context.SaveChanges();
            return genre;
        }

        // Check if a genre with the specified ID exists
        public Task<bool> AnyAsync(byte id)
        {
            return _context.Genres.AnyAsync(g => g.Id == id);
        }

        // Retrieve all genres from the database
        public async Task<IEnumerable<Genre>> GetAllAsync()
        {
            return await _context.Genres.OrderBy(g => g.Name).ToListAsync();
        }

        //  Retrieve a genre by its ID
        public async Task<Genre> GetByIdAsync(byte id)
        {
            return await _context.Genres.SingleOrDefaultAsync(g => g.Id == id);
        }

        // Remove a genre from the database.
        public Genre Remove(Genre genre)
        {
            _context.Genres.Remove(genre);
            _context.SaveChanges();
            return genre;
        }

        // /Update a genre in the database.
        public Genre Update(Genre genre)
        {
            _context.Update(genre);
            _context.SaveChanges();
            return genre;
        }
    }
}
