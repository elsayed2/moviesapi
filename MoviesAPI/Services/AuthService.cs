﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Security.Cryptography;

namespace MoviesAPI.Services
{
    public class AuthService : IAuthService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly JWT _jwt;
        public AuthService(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IOptions<JWT> jwt)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _jwt = jwt.Value;
        }


        // Generate a JWT for the given user, based on their claims, roles, and other information.
        public async Task<JwtSecurityToken> GenerateTokenAsync(ApplicationUser user)
        {
            // Get the claims associated with the user
            var userClaims = await _userManager.GetClaimsAsync(user);

            // Get the roles of the user
            var roles = await _userManager.GetRolesAsync(user);

            // Create claims for the user's roles
            var rolesClaims = new List<Claim>();

            foreach (var role in roles)
                rolesClaims.Add(new Claim("roles", role));

            // Combine the user claims, roles claims, and additional claims
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub,user.UserName),
                new Claim(JwtRegisteredClaimNames.Email,user.Email),
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
                new Claim("uid",user.Id)
            }.Union(userClaims).Union(rolesClaims);

            // Create a symmetric security key using the JWT secret key
            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwt.Key));

            // Create signing credentials using the symmetric security key and HMACSHA256 algorithm
            var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);

            // Generate the JWT security token
            return new JwtSecurityToken(
                _jwt.Issuer,
                _jwt.Audience,
                claims,
                null,
                DateTime.Now.AddMinutes(_jwt.DurationInMinutes),
                signingCredentials);

        }

        private RefreshToken CreateRefreshToken()
        {
            var randomNumber = new byte[32];
            using var generator = new RNGCryptoServiceProvider();
            generator.GetBytes(randomNumber);

            return new RefreshToken
            {
                Token = Convert.ToBase64String(randomNumber),
                CreatedOn = DateTime.UtcNow,
                ExpiresOn = DateTime.UtcNow.AddDays(10)
            };
        }


        // Retrieve an authentication token for a given token request model.
        public async Task<AuthModel> GetTokenAsync(TokenRequestModel model)
        {
            //Retrieve the user with the provided email from the user manager
            var user = await _userManager.FindByEmailAsync(model.Email);

            // Check if the user is null or the provided password is incorrect
            if (user is null || !await _userManager.CheckPasswordAsync(user, model.Password))
                return new AuthModel { Message = "Email or password is incorrect" };

            // Retrieve the roles associated with the user
            var userRoles = await _userManager.GetRolesAsync(user);

            // Generate a JWT security token for the user
            var jwtSecurityToken = await GenerateTokenAsync(user);

            var authModel= new AuthModel
            {
                Username = user.UserName,
                Email = user.Email,
                Token = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken),
                IsAuthenticated = true,
                ExpiresOn=jwtSecurityToken.ValidTo,
                Roles = userRoles.ToList()
            };


            if (user.RefreshTokens.Any(r=> r.IsActive))
            {
                var activeRefreshToken = user.RefreshTokens.FirstOrDefault(r => r.IsActive);
                authModel.RefreshToken = activeRefreshToken.Token;
                authModel.RefreshTokenExpiration = activeRefreshToken.ExpiresOn;
            }
            else
            {
                var refreshToken = CreateRefreshToken();
                authModel.RefreshToken = refreshToken.Token;
                authModel.RefreshTokenExpiration = refreshToken.ExpiresOn;

                user.RefreshTokens.Add(refreshToken);
                await _userManager.UpdateAsync(user);
            }

            
            return authModel;
        }


        // Handle user registration. 
        public async Task<AuthModel> RegisterAsync(RegisterModel model)
        {
            // Check if the email is already registered in the database
            if (await _userManager.FindByEmailAsync(model.Email) != null)
                return new AuthModel { Message = "Email is already registered" };

            // Check if the username is already registered in the database
            if (await _userManager.FindByNameAsync(model.Username) is not null)
                return new AuthModel { Message = "Username is already registered" };

            // Create a new ApplicationUser object and populate it with the registration data
            var user = new ApplicationUser
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                UserName = model.Username,
                Email = model.Email
            };

            var errors = string.Empty;

            // Create a new user using the UserManager
            var result = await _userManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                    errors += $"{error.Description}, ";

                return new AuthModel { Message = errors };
            }

            // Assign the "User" role to the newly created user
            await _userManager.AddToRoleAsync(user, "User");

            // Generate a JWT security token for the user
            var jwtSecurityToken = await GenerateTokenAsync(user);

            // Generate a refresh token and save it in the database
            var refreshToken = CreateRefreshToken();
            user.RefreshTokens?.Add(refreshToken);
            await _userManager.UpdateAsync(user);

            // Return a response with the user information, JWT token, and authentication status
            return new AuthModel
            {
                Username = user.UserName,
                Email = user.Email,
                Roles = new List<string> { "User" },
                Token = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken),
                ExpiresOn = jwtSecurityToken.ValidTo,
                IsAuthenticated = true,
                RefreshToken = refreshToken.Token,
                RefreshTokenExpiration = refreshToken.ExpiresOn
            };
        }

        // Add a role to a user.
        public async Task<string> AddRoleAsync(AddRoleModel model)
        {
            // Retrieve the user by their Id.
            var user = await _userManager.FindByIdAsync(model.UserId);

            // Check if the user or the specified role does not exist.
            if (user is null || !await _roleManager.RoleExistsAsync(model.Role))
                return "Invalid user Id or role";

            // Check if the user is already assigned to the specified role.
            if (await _userManager.IsInRoleAsync(user, model.Role))
                return "User is already assigned to this role";

            // Add the user to the specified role.
            var result = await _userManager.AddToRoleAsync(user, model.Role);

            return result.Succeeded ? "" : "Something wont wrong";
        }

        // Refresh the authentication token for a user using the provided refresh token
        public async Task<AuthModel> RefreshTokenAsync(string refreshToken)
        {
            var authModel = new AuthModel();

            // Find the user with the matching refresh token
            var user = _userManager.Users.Single(u => u.RefreshTokens.Any(r => r.Token == refreshToken));
            if (user == null)
            {
                authModel.IsAuthenticated = false;
                authModel.Message = "Invalid token";
                return authModel;
            }

            // Find the specific refresh token for the user
            var oldRefreshToken = user.RefreshTokens.Single(u => u.Token == refreshToken);
            if (!oldRefreshToken.IsActive)
            {
                authModel.IsAuthenticated = false;
                authModel.Message = "Inactive token";
                return authModel;
            }

            // Revoke the old refresh token
            oldRefreshToken.RevokedOn = DateTime.UtcNow;

            // Create a new refresh token
            var newRefreshToken = CreateRefreshToken();
            user.RefreshTokens.Add(newRefreshToken);
            await _userManager.UpdateAsync(user);

            // Generate a new JWT token for the user
            var jwtToken = await GenerateTokenAsync(user);

            // Populate the AuthModel object with the refreshed authentication information
            authModel.IsAuthenticated = true;
            authModel.Email = user.Email;
            authModel.Username = user.UserName;
            var userRoles = await _userManager.GetRolesAsync(user);
            authModel.Roles = userRoles.ToList();
            authModel.Token = new JwtSecurityTokenHandler().WriteToken(jwtToken);
            authModel.ExpiresOn = jwtToken.ValidTo;
            authModel.RefreshToken = newRefreshToken.Token;
            authModel.RefreshTokenExpiration = newRefreshToken.ExpiresOn;

            return authModel;
        }

        // Revoke a refresh token for a user.
        public async Task<bool> RevokeTokenAsync(string token)
        {
            // Find the user associated with the token
            var user = _userManager.Users.SingleOrDefault(u =>
            u.RefreshTokens.Any(r => r.Token == token));

            if (user == null)
                return false;

            // Retrieve the specific refresh token
            var refreshToken = user.RefreshTokens.Single(t => t.Token == token);

            if (!refreshToken.IsActive)
                return false;

            // Revoke the refresh token
            refreshToken.RevokedOn = DateTime.UtcNow;
            await _userManager.UpdateAsync(user);
            
            return true;
        }
    }
}
