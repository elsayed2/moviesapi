﻿
namespace MoviesAPI.Services
{
    public class MoviesService : IMoviesService
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        public MoviesService(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // Add a new movie to the database.
        public async Task<MovieDetailsDTO> AddAsync(Movie movie)
        {
            // Add the movie to the context and save changes
            await _context.Movies.AddAsync(movie);
            _context.SaveChanges();

            // Map the Movie object to a MovieDetailsDTO object and return it
            return await MovieDetailsAsync(movie);
        }

        // Retrieve all movies from the database or movies based on the specified genre.
        public async Task<IEnumerable<MovieDetailsDTO>> GetAsync(byte genreId = 0)
        {
            var movies = await _context.Movies
                .Where(m=> m.GenreId==genreId||genreId==0)
                .OrderByDescending(m => m.Rate)
                .Include(m => m.Genre)
                .Select(m => new MovieDetailsDTO
                {
                    Id = m.Id,
                    Title = m.Title,
                    Year = m.Year,
                    Rate = m.Rate,
                    Poster = m.Poster,
                    Storyline = m.Storyline,
                    GenreName = m.Genre.Name
                }).ToListAsync();

            return movies;
        }

        // Retrieve a movie from the database by its ID.
        public async Task<Movie> GetByIdAsync(int id)
        {
            // Retrieve the movie from the database including its associated genre
             return await _context.Movies
                .Include(m => m.Genre)
                .SingleOrDefaultAsync(m => m.Id == id);
        }

        // Retrieve the detailed information of a movie, including its genre name.
        public async Task<MovieDetailsDTO> MovieDetailsAsync(Movie movie)
        {
            // Map the Movie object to a MovieDetailsDTO object
            var moviesDetails = _mapper.Map<MovieDetailsDTO>(movie);

            // Retrieve the genre name for the specified movie title
            moviesDetails.GenreName = await _context.Movies
                .Where(m => m.Id == movie.Id)
                .Include(m => m.Genre)
                .Select(m => m.Genre.Name)
                .FirstOrDefaultAsync();

            return moviesDetails;
        }

        // Remove a movie from the database.
        public async Task<Movie> RemoveAsync(Movie movie)
        {
            // Remove the movie from the context
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
            return movie;
        }

        // Update the details of a movie in the database.
        public async Task<Movie> UpdateAsync(Movie movie, MovieUpdateDTO dto)
        {
            movie.Title = dto.Title;
            movie.Rate = dto.Rate;
            movie.Storyline = dto.Storyline;
            movie.Year = dto.Year;
            movie.GenreId = dto.GenreId;

            _context.Update(movie);
            await _context.SaveChangesAsync();

            return movie;
        }



    }
}
