﻿
namespace MoviesAPI.Helpers
{
    public class MappingProfile:Profile
    {
        public MappingProfile()
        {
            CreateMap<MovieCreateDTO, Movie>()
              .ForMember(dest => dest.Poster, src => src.Ignore());

            CreateMap<Movie, MovieDetailsDTO>()
                .ForMember(dest => dest.GenreName, src => src.MapFrom(src=> src.Genre.Name));
        }
    }
}
