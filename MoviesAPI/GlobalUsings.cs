﻿global using MoviesAPI.Models;
global using MoviesAPI.DTOs;
global using MoviesAPI.Services;
global using MoviesAPI.Helpers;
global using MoviesAPI.Identity;
global using Microsoft.EntityFrameworkCore;
global using System.ComponentModel.DataAnnotations.Schema;
global using System.ComponentModel.DataAnnotations;
global using Microsoft.AspNetCore.Mvc;
global using Microsoft.AspNetCore.Identity;
global using AutoMapper;