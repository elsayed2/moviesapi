﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace MoviesAPI.Identity
{
    public class IdentityContext :IdentityDbContext<ApplicationUser>
    {
        public IdentityContext(DbContextOptions<IdentityContext> options):base(options)
        {

        }
    }
}
