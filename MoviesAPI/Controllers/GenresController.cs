﻿using Microsoft.AspNetCore.Authorization;
using MoviesAPI.Services;

namespace MoviesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GenresController : ControllerBase
    {
        private readonly IGenresService _service;
        public GenresController(IGenresService service)
        {
            _service=service;
        }

        // Retrieve all genres asynchronously.
        [HttpGet]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> GetAllAsync()
        {
            // Returns a response with all genres ordered by name.
            return Ok(await _service.GetAllAsync());
        }

        // Create a new genre.
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> CreateAsync(GenreDTO dto)
        {
            var genre = new Genre { Name = dto.Name };
            return Ok(await _service.AddAsync(genre));
        }

        // Retrieve a genre with the specified id.
        [HttpGet("{id}")]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> GetGenreByIdAsync(byte id)
        {
            var genre = await _service.GetByIdAsync(id);
            if(genre==null)
                return NotFound($"No genre was found with the ID {id}");

            return Ok(genre);
        }

        // Update the details of a genre with the specified ID.
        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UpdateAsync(byte id, [FromBody] GenreDTO dto)
        {
            var genre = await _service.GetByIdAsync(id);
            if (genre == null)
                return NotFound($"No genre was found with the ID {id}");

            genre.Name = dto.Name;
            return Ok(_service.Update(genre));
        }

        // Delete a genre with the specified ID.
        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteAsync(byte id)
        {
            var genre = await _service.GetByIdAsync(id);
            if(genre==null)
                return NotFound($"No genre was found with the ID {id}");

            return Ok(_service.Remove(genre));
        }



    }
}
