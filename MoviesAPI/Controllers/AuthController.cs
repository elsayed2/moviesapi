﻿using Microsoft.AspNetCore.Mvc;

namespace MoviesAPI.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;
        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }


        // Register a new user
        [HttpPost("register")]
       public async Task<IActionResult> RegisterAsync([FromBody]RegisterModel model)
       {
            if(!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await _authService.RegisterAsync(model);

            if (!result.IsAuthenticated)
                return BadRequest(result.Message);

            SetRefreshTokenInCookie(result.RefreshToken, result.RefreshTokenExpiration);

            return Ok(result);
        }

        // Generate a token for the provided credentials.
        [HttpPost("token")]
        public async Task<IActionResult> GetTokenAsync([FromBody] TokenRequestModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await _authService.GetTokenAsync(model);

            if (!result.IsAuthenticated)
                return BadRequest(result.Message);

            if (!string.IsNullOrEmpty(result.RefreshToken))
                SetRefreshTokenInCookie(result.RefreshToken, result.RefreshTokenExpiration);

            return Ok(result);

        }

        // Refresh an access token using a refresh token
        [HttpPost("refreshToken")]
        public async Task<IActionResult> RefreshTokenAsync()
        {
            // Retrieve the refresh token from the request cookie
            var refreshToken = Request.Cookies["refreshToken"];

            // Call the authentication service to refresh the token
            var result = await _authService.RefreshTokenAsync(refreshToken);

            if (!result.IsAuthenticated)
                return BadRequest(result.Message);

            // Set the new refresh token and its expiration in the response cookie
            SetRefreshTokenInCookie(result.RefreshToken, result.RefreshTokenExpiration);

            return Ok(result);
        }

        // Revoke the specified token
        [HttpPost("revokeToken")]
        public async Task<IActionResult> RevokeTokenAsync(RevokeToken model)
        {
            var token = model.Token ?? Request.Cookies["refreshToken"];

            if (string.IsNullOrEmpty(token))
                return BadRequest("Token is required");

            var result = await _authService.RevokeTokenAsync(token);

            if (!result)
                return BadRequest("Invalid Token");

            return Ok();

        }

        // Adds a role
        [HttpPost("AddRole")]
        public async Task<IActionResult> AddRoleAsync(AddRoleModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await _authService.AddRoleAsync(model);

            if(!string.IsNullOrEmpty(result))
                return BadRequest(result);

            return Ok(model);

        }

        // Set the refresh token in a cookie with the provided expiration date.
        private void SetRefreshTokenInCookie(string refreshToken,DateTime expiresOn)
        {
            var cookeOptions = new CookieOptions
            {
                HttpOnly = true,
                Expires = expiresOn.ToLocalTime()
            };

            Response.Cookies.Append("refreshToken", refreshToken, cookeOptions);
        }
    }
}
