﻿using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;

namespace MoviesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly IMoviesService _moviesService;
        private readonly IGenresService _genresService;
        private readonly IMapper _mapper;

        // Define the allowed file extensions and maximum allowed size (in bytes) for image uploads.
        private List<string> _allowedExtensions=new List<string>{".jpg",".png"};
        private long _maxAllowedSize = 1048576;

        public MoviesController(IMoviesService moviesService,IGenresService genresService,IMapper mapper)
        {
            _moviesService = moviesService;
            _genresService = genresService;
            _mapper = mapper;
        }


        // Retrieve all movies with their details ordered by rate in descending order
        [HttpGet]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> GetAllAsync()
        {
            return Ok(await _moviesService.GetAsync());
        }

        // Create a new movie
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> CreateAsync([FromForm]MovieCreateDTO dto)
        {
            // Check if the poster image is valid
            if (!IsValidPoster(dto.Poster))
                return BadRequest("The image has an invalid size or extension.");

            // Check if the genre ID is valid
            if (!await _genresService.AnyAsync(dto.GenreId))
                return BadRequest("The genre ID is invalid.");

            // Map the DTO to a Movie object
            var movie = _mapper.Map<Movie>(dto);

            // Convert the poster image to a byte array and store it in the Movie object 
            using var stream=new MemoryStream();
            await dto.Poster.CopyToAsync(stream);
            movie.Poster = stream.ToArray();

            //Add the movie to the context and return the movie details with the genre name
            return Ok(await _moviesService.AddAsync(movie));
        }


        // Retrieve a movie by its ID.
        [HttpGet("{id}")]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> GetByIdAsync(int id)
        {

            // Retrieve the movie from the database including its associated genre
            var movie = await _moviesService.GetByIdAsync(id);

            if (movie == null)
                return NotFound($"No movie was found with the ID {id}");

            // Map the movie to a MovieDetailsDTO object
            var movieDetails = _mapper.Map<MovieDetailsDTO>(movie);

            return Ok(movieDetails);

        }


        // Retrieves movies by genre ID.
        [HttpGet("GetByGenreId")]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> GetByGenreIdAsync(byte genreId)
        {
            // Retrieve movies with the specified genre ID, ordered by rate in descending order
            var movies = await _moviesService.GetAsync(genreId);

            // Check if there are no movies for the genre ID
            if (movies == null)
                return NotFound($"No movies for this genre ID {genreId}");

            // Map movies to MovieDetailsDTO objects
            var moviesDetails = _mapper.Map<IEnumerable<MovieDetailsDTO>>(movies);

            return Ok(moviesDetails);
        }

        // Delete a movie by its ID.
        [HttpDelete]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            // Find the movie by its ID
            var movie = await _moviesService.GetByIdAsync(id);

            // Check if the movie is not found
            if (movie==null)
                return NotFound($"No movie was found with the ID {id}");

            // Remove the movie from the context
            return Ok(await _moviesService.RemoveAsync(movie));
        }

        // Update a movie by its ID.
        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UpdateAsync(int id, [FromForm] MovieUpdateDTO dto)
        {
            // Find the movie by its ID
            var movie = await _moviesService.GetByIdAsync(id);

            // Check if the movie is not found
            if (movie == null)
                return NotFound($"No movie was found with the ID {id}");

            // Check if any genre exists with the specified ID in the database
            var isValidGenre = await _genresService.AnyAsync(dto.GenreId);
            if (!isValidGenre)
                return BadRequest("Invalid genre ID");

            // Update the movie properties
            if (dto.Poster!=null)
            {
                var isValidPoster = IsValidPoster(dto.Poster);
                if(isValidPoster)
                {
                    using var stream = new MemoryStream();
                    await dto.Poster.CopyToAsync(stream);
                    movie.Poster = stream.ToArray();
                }
            }

            // Update the movie in the database and return it
            return Ok(await _moviesService.UpdateAsync(movie, dto));
        }

        // Validate the uploaded poster file based on its extension and size.
        private bool IsValidPoster(IFormFile poster)
        {
            
            return _allowedExtensions.Contains(Path.GetExtension(poster.FileName).ToLower()) 
                && poster.Length < _maxAllowedSize;
            
        }
    }
}
