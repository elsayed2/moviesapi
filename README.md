# Movies API



## Description
This code represents a WebAPI project with three controllers: GenresController, MovieController, and AuthController.

The GenresController handles operations related to genres. It provides endpoints to retrieve all genres, create a new genre, update an existing genre by ID, and delete a genre by ID.

The MovieController handles operations related to movies. It provides endpoints to create a new movie, retrieve all movies, retrieve a movie by ID, retrieve movies by genre ID, update a movie by ID, and delete a movie by ID.
The MovieController includes additional functionality related to file upload and validation. It checks the file extension and size of the uploaded movie posters to ensure they meet the specified requirements.

The AuthController handles user authentication and authorization operations for the MoviesAPI application using JWT Tokens and JWT Refresh Tokens. It provides endpoints for registering new users, generating access tokens, refreshing tokens, revoking tokens, and managing user roles.

To improve code organization and maintainability, services have been implemented to separate the code responsible for interacting with the databases.

 ![swagger](/WebAPI.jpg)

# GenresController

This controller handles genres in the WebAPI.

## Endpoints

### Get all genres

- **URL:** `/api/genres`
- **Method:** GET
- **Description:** Retrieves all genres.
- **Returns:** A list of genres.

### Create a new genre

- **URL:** `/api/genres`
- **Method:** POST
- **Description:** Creates a new genre.
- **Request Body:** Genre data in the form of a GenreDTO object.
- **Returns:** The created genre.

### Update an existing genre

- **URL:** `/api/genres/{id}`
- **Method:** PUT
- **Description:** Updates an existing genre specified by ID.
- **Request Body:** Genre data in the form of a GenreDTO object.
- **Returns:** The updated genre.

### Delete a genre

- **URL:** `/api/genres/{id}`
- **Method:** DELETE
- **Description:** Deletes a genre specified by ID.
- **Returns:** The deleted genre.

# MovieController

This controller handles movies in the WebAPI.

## Endpoints

### Create a new movie

- **URL:** `/api/movies`
- **Method:** POST
- **Description:** Creates a new movie.
- **Request Body:** Movie data in the form of a CreateMovieDTO object.
- **Returns:** The created movie.

### Get all movies

- **URL:** `/api/movies`
- **Method:** GET
- **Description:** Retrieves all movies.
- **Returns:** A list of movies.

### Get a movie by ID

- **URL:** `/api/movies/{id}`
- **Method:** GET
- **Description:** Retrieves a movie specified by ID.
- **Returns:** The movie details.

### Get movies by genre ID

- **URL:** `/api/movies/GetByGenreId`
- **Method:** GET
- **Description:** Retrieves movies based on the specified genre ID.
- **Returns:** A list of movies.

### Update a movie

- **URL:** `/api/movies/{id}`
- **Method:** PUT
- **Description:** Updates an existing movie specified by ID.
- **Request Body:** Movie data in the form of an UpdateMovieDTO object.
- **Returns:** The updated movie.

### Delete a movie

- **URL:** `/api/movies/{id}`
- **Method:** DELETE
- **Description:** Deletes a movie specified by ID.
- **Returns:** The deleted movie.

development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

# AuthController (Controller for Authentication)

This controller handles user authentication and authorization operations for the MoviesAPI application. It provides endpoints for registering new users, generating access tokens, refreshing tokens, revoking tokens, and managing user roles.

## Endpoints:

### Register a new user: [POST] /api/Auth/register
Accepts a JSON payload containing user registration information.
Validates the input data and returns a success response with the registered user's information and access token.

### Generate a token: [POST] /api/Auth/token
Accepts a JSON payload containing user credentials.
Validates the input data and returns an access token if the provided credentials are valid.

### Refresh an access token: [POST] /api/Auth/refreshToken
Retrieves the refresh token from the request cookie.
Calls the authentication service to refresh the access token.
Sets the new refresh token in the response cookie.

### Revoke a token: [POST] /api/Auth/revokeToken
Accepts a JSON payload containing the token to revoke or retrieves the refresh token from the request cookie.
Calls the authentication service to revoke the specified token.

### Add a role: [POST] /api/Auth/AddRole
Accepts a JSON payload containing the role to add.
Validates the input data and adds the role to the user.
Dependencies:

## IAuthService: 
An interface for the authentication service that provides methods for user registration, token generation, token refreshing, token revocation, and role management.

## Note:
This controller relies on an implementation of the IAuthService interface to perform the actual authentication and authorization operations.
